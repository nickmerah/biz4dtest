<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StartController;
use App\Http\Controllers\StartUpController;
use App\Http\Controllers\UserController;
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('reg', [StartUpController::class, 'index']);
Route::resource('start', StartController::class)->middleware('XssSanitization');
Route::get('success', function () { return view('finish');});
Route::post('dologin', [StartController::class, 'login'])->name('dologin')->middleware('XssSanitization');
Route::resource('dashboard', UserController::class);
Route::get('changepassword', [UserController::class, 'changepassword']);
Route::post('changepass', [UserController::class, 'changepass'])->name('changepass');
Route::get('logout', [UserController::class, 'logout']);
Route::get('projects', [UserController::class, 'projects']);
Route::get('newproject', [UserController::class, 'newproject']);
Route::post('newproj', [UserController::class, 'newproj'])->name('newproj');
Route::get('viewproj/{pid}', [UserController::class, 'viewproject']);
Route::get('editproj/{pid}', [UserController::class, 'editproject']);
Route::post('upproj', [UserController::class, 'upproj'])->name('upproj');
Route::get('projectusers', [UserController::class, 'projectusers']);
Route::post('newprojuser', [UserController::class, 'newprojuser'])->name('newprojuser');
Route::get('removeproj/{id}', [UserController::class, 'removeproj'])->name('removeproj');
Route::get('assignprojects', [UserController::class, 'assignprojects']);
Route::get('survey', [UserController::class, 'survey']);
Route::post('surveytest', [UserController::class, 'surveytest'])->name('surveytest');
//admin
Route::get('alogin', [StartUpController::class, 'alogin']);
Route::post('adlogin', [StartController::class, 'adminlogin'])->name('adlogin')->middleware('XssSanitization');
Route::get('admindashboard', [UserController::class, 'admindashboard']);
Route::get('aprojects', [UserController::class, 'aprojects']);
Route::get('aviewproj/{pid}', [UserController::class, 'aviewproject']);
Route::get('aeditproj/{pid}', [UserController::class, 'aeditproject']);
Route::post('aupproj', [UserController::class, 'aupproj'])->name('aupproj');
Route::get('deleteproj/{id}', [UserController::class, 'deleteproj'])->name('deleteproj');
Route::get('aprojectusers', [UserController::class, 'aprojectusers']);
Route::get('removeuser/{id}', [UserController::class, 'removeuser'])->name('removeuser');


 