<!doctype html>
<html lang="en-US">
	
 
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
		<title>.:: My Projects</title>
		
		<!-- Optional - Google font -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Gafata" />
		
		<!-- Required - Icon font -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css') }} " />
		
		<!-- Required - Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}" /> 
		
		<!-- Required - Form style -->
		<link rel="stylesheet" href="{{asset('css/flat-form.css') }}" /> 
		
		<!-- NOT required - Page style -->
<link rel="stylesheet" href="{{asset('css/page-style.css') }}" /> 
<script type="text/javascript" src="{{asset('js/jquery-1.11.3-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/validation.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/script.js') }}"></script>
	</head>
	
	<body>
		<div class="container">
			<header class="header">
				<h1>My Projects</h1>
			</header>
			
			<div class="content">
				<aside class="col-sm-4 col-md-3 sidebar">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="{{env('APP_URL')}}">Home</a> </li>
                        <li><a href="reg">Register</a> </li>
                         <li><a href="alogin">Admin Login</a> </li>
					</ul>
				</aside>
				
				<article class="col-sm-8 col-md-9">
				
					<!-- START Login form -->
					
					<div class="row">
					
						<!-- Login form -->
						<div class="col-md-6">
							<div class="panel panel-form">
								<!-- Form header -->
								<div class="panel-heading">
									<h2 class="title">Login</h2>
									<p>Don't have an account? <a href="reg">Create one</a>.</p>
								</div>
								
								<div class="panel-body">
					
        
       
       @if ($errors->any())		 <hr />
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                {{ $error }} 
            @endforeach
        </ul>
    </div>
@endif
                                    <form role="form" class="form-signin" method="post" id="login-form" action="{{ route('dologin') }}"   autocomplete="off">
      
       @csrf
       
        
        
										<!-- Username or email -->
										<div class="form-group">
											<label for="email" class="control-label">Email</label>
											<div class="has-feedback">
												<input type="email" class="form-control" id="email" name = "email" autocomplete= "off" value="{{ old('email') }}"/>
												<span class="fa fa-user form-control-feedback" aria-hidden="true"></span>
                                                  <span id="check-e"></span>
											</div>
										</div>
										
										<!-- Password -->
										<div class="form-group">
											<label for="password" class="control-label">Password</label>
											<div class="has-feedback">
												<input type="password" class="form-control" id="password" name="password"/>
												<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
											</div>
										</div>
										
										<!-- Remember checkbox -->
										<div class="form-group">
											<div class="checkbox">
												<label class="custom-option toggle" data-off="OFF" data-on="ON">
													<input type="checkbox" id="remember" name="remember" value="1" />
													<span class="button-checkbox"></span>
												</label>
												<label for="remember">Remember Me</label>
											</div>
										</div>
										
										<!-- Logun button -->
										<div class="form-group">
											<button type="submit" class="btn btn-primary">Login</button>
											
											 
										</div>
									</form>
								</div>
							</div>
						</div>
						
						<!-- Login with social network -->
						<div class="col-md-6">
							<h2 class="text-center">Login with a social network</h2>
							
							<button type="button" class="btn btn-block btn-facebook btn-lg btn-left-icon">
								<span class="fa fa-facebook" aria-hidden="true"></span> Login with Facebook
							</button>
							
							<button type="button" class="btn btn-block btn-twitter btn-lg btn-left-icon">
								<span class="fa fa-twitter" aria-hidden="true"></span> Login with Twitter
							</button>
							
							<button type="button" class="btn btn-block btn-google btn-lg btn-left-icon">
								<span class="fa fa-google-plus" aria-hidden="true"></span> Login with Google Plus
							</button>
						</div>
						
					</div>
					
					<!-- END Login form -->
					
				</article>
				
				<div class="clearfix"></div>
			</div>
		</div>
		
		<!-- Required - jQuery -->
		<script src="{{asset('js/bootstrap.min.js') }}"></script>
		
		<!-- Required - Custom select -->
		<script src="{{asset('js/fancySelect.js') }}"></script>
		
		<script>
			$(document).ready(function() {
				$('.custom-select').fancySelect(); // Custom select
				$('[data-toggle="tooltip"]').tooltip() // Tooltip
			});
		</script> 
	</body>

 
</html>