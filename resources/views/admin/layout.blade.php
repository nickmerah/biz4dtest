<!doctype html>
<html lang="en-US">
	
 
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
		<title>.:: My Projects</title>
		
		<!-- Optional - Google font -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Gafata" />
		
		<!-- Required - Icon font -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css') }} " />
		
		<!-- Required - Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}" /> 
		
		<!-- Required - Form style -->
		<link rel="stylesheet" href="{{asset('css/flat-form.css') }}" /> 
		
		<!-- NOT required - Page style -->
<link rel="stylesheet" href="{{asset('css/page-style.css') }}" /> 
<script type="text/javascript" src="{{asset('js/jquery-1.11.3-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/validation.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/script.js') }}"></script>
	</head>
	
	<body>
		<div class="container">
			<header class="header">
				<h1>My Projects</h1>
			</header>
			
			<div class="content">
				<aside class="col-sm-4 col-md-3 sidebar">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="{{  url('admindashboard') }}"><i class="fa fa-home fa-fw"></i>Home</a> </li>
                         <li> <a href="{{  url('aprojects') }}"> <i class="fa fa-lock fa-fw"></i> Projects</a></li>
                         <li> <a href="{{  url('aprojectusers') }}"> <i class="fa fa-user fa-fw"></i> Users</a></li>
                          <li> <a href="{{  url('logout') }}"> <i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
					</ul>
				</aside>
    @foreach ($users as $user)
   
           @php
             $fname = $user->fname;  
             $email  = $user->email ;
             $phoneno = $user->phoneno;
                @endphp
         
  @endforeach  			
				<article class="col-sm-8 col-md-9">
				
					<!-- START Login form -->
					
					<div class="row">
					
						<!-- Login form -->
						<div class="col-md-12">
							<div class="panel panel-form">
								<!-- Form header -->
								 
								
								<div class="panel-body">
					
        
       <fieldset>
							<legend>Welcome  {!! $fname !!}</legend>
        
         @yield('contents')
        
         <!-- /#page-wrapper -->
<div class="clearfix"></div>
			</div>
		</div>
		
		<!-- Required - jQuery -->
		<script src="{{asset('js/bootstrap.min.js') }}"></script>
		
		<!-- Required - Custom select -->
		<script src="{{asset('js/fancySelect.js') }}"></script>
		
		<script>
			$(document).ready(function() {
				$('.custom-select').fancySelect(); // Custom select
				$('[data-toggle="tooltip"]').tooltip() // Tooltip
			});
		</script> 
	</body>

 
</html>