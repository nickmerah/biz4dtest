@extends('admin/layout')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
 
 
  @foreach ($surveys  as $survey )					  
@php
       $sdatas[]  =   "['$survey->answer',        $survey->count],";
            
       
   @endphp   
    
   	 @endforeach 
       
     
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Project Name', 'Details'],
          @php
    echo  $sdata = implode(" ",$sdatas);
     
      @endphp
        ]);

        var options = {
          title: 'Projects'
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

        chart.draw(data, options);
      }
    </script>
   
    @foreach ($projs  as $proj )					  
@php
       $pdatas[]  =   "['$proj->pname',     $proj->count],";
            
           
   @endphp   
    
   	 @endforeach 
     
      
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Project Name', 'Details'],
          @php
    echo  $pdata = implode(" ",$pdatas);
     
      @endphp
        ]);

        var options = {
          title: 'Projects'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

@section('contents')	


							
							<!-- Default, grey -->
							<div class="alert alert-default alert-dismissible" role="alert">
								 
								<strong>This is your personalized dashboard<br />
                                Use the left navigation bar to access functionalities of the system.</strong>  
							</div>
							
							<!-- Success, green -->
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>User</strong> <br />You are currently  {{ $numusers }} User(s)
							</div>
							
							<!-- Info, purple -->
							<div class="alert alert-info alert-dismissible" role="alert">
								 
								<strong>Projects</strong><br />There are currently  {{ $numproj }} Projects(s)
							</div>
							
							<!-- Warning, yellow -->
							<div class="alert alert-warning alert-dismissible" role="alert">
								 
								<strong>Survey</strong><br /> 
                                <div id="chart_div" style="width: 720px; height: 500px;"></div>
							</div>
							
							<!-- Danger, red -->
							<div class="alert alert-danger alert-dismissible" role="alert">
								 
									<strong>Projects Graph</strong> 
                                    <br /><div id="piechart" style="width: 720px; height: 500px;"></div>
							</div>
						</fieldset>
                          
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				