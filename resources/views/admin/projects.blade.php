@extends('admin/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>Projects</strong>  
                             
							</div>
                            
                            
							<div class="row">
                           
								
                             
						 
										<div class="table-responsive">
                                        
						    <table class="table" border="1" width="100%">
    <thead>
      <tr>
        <th>S/N</th>
        <th>Name</th>
         <th>Description</th>
        <th>Status</th>
        <th>Action</th>
          
        </tr>
    </thead>
    <tbody style="font-size:13px">
	@foreach ($projects  as $project )					  

<tr>
        <td>{{$loop->iteration}}</td>
        <td>{{ $project->pname }}   </td>
         <td>{{ $project->pdesc }}   </td>
        <td>{{ $project->pstatus }}</td>
        <td><a href="aviewproj/{{ $project->pid }}"><i class="fa fa-eye"></i>View</a>  | <a href="aeditproj/{{ $project->pid }}"><i class="fa fa-edit"></i>Edit</a> | <a href="deleteproj/{{ $project->pid }}"><i class="fa fa-edit"></i>Delete</a></td>
      </tr>
       @endforeach  
	
    </tbody>
  </table>

</div>
									</div>
							 
							
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				