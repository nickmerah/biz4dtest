@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>Add A New Project</strong>   <br />
                                Fill out the form below to add a new project
                             
							</div>
                            
                            
							<div class="row">
                            
                           
							 <form role="form" name="form1" id="form1" method="post" action="{{ route('newproj') }}"  autocomplete="off"> @csrf
						@if ($errors->any())
       
       <div class="row" style="color:#F00">
            <div class="m12 s12 col">
                <div class="card-panel red darken-1">
                    <div class="row">
                        <div class="col l8 white-text">
                            <h5>Error!</h5>
                           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   
@endif			<!-- Name and gender -->
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Name <span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<input type="text" class="form-control" id="pname"  name="pname" required/>
                                    
                                           
										  </div>
										</div>
										
										 
									</div>
									
									
									
									
									
								
					    <div class="form-group">
										<label for="desc" class="control-label">Description <span class="required-field">*</span></label>
										<div class="has-feedback">
                                            <textarea name="pdesc" class="form-control" id="pdesc" required></textarea>
											
	    </div>
									</div>
                                    
                                    
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Upload Image<span class="required-field">*</span></label>
										<div class="has-feedback">
                                                <input name="pimage" type="file" id="pimage" required class="form-control" accept="image/jpeg">
	    </div>
									</div>
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Upload Video <span class="required-field">*</span></label>
										<div class="has-feedback">
                                                <input name="pvideo" type="file" id="pvideo" required class="form-control"  accept="video/x-mpeg2">
									</div>
                                    </div>
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Project Document (Optional)</label>
										<div class="has-feedback">
                                             <input name="pdoc" type="file" id="pdoc" class="form-control" accept="application/pdf">
	    </div>
									</div>
									 
									 <!-- Phone Number -->
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Github link<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<input type="url" class="form-control" id="gitlink" name="gitlink" required/>
                                            
											</div>
										</div>
										
										 
									</div>
									 
							<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Status<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="pstatus" id="pstatus" >
<option value="">Select Project Status </option>  
<option value="Active">Active</option> 
<option value="Incomplete">Incomplete</option> 
<option value="Ongoing">Ongoing</option> 
<option value="Stuck">Stuck</option> 
	 
                                                     
  </select>
											</div>
										</div>
										
										 
									</div>		
									 
									
									<!-- Create button -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Create Project</button>
									</div>
								</form>
                             
						 
										
									</div>
							 
							
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				