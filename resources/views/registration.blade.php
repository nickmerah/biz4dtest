<!doctype html>
<html lang="en-US">
	
 
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
		<title>.:: My Projects</title>
		
		<!-- Optional - Google font -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Gafata" />
		
		<!-- Required - Icon font -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css') }} " />
		
		<!-- Required - Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}" /> 
		
		<!-- Required - Form style -->
		<link rel="stylesheet" href="{{asset('css/flat-form.css') }}" /> 
		
		<!-- NOT required - Page style -->
<link rel="stylesheet" href="{{asset('css/page-style.css') }}" /> 
<script type="text/javascript" src="{{asset('js/jquery-1.11.3-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/validation.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/script.js') }}"></script>
	</head>
	
	<body>
		<div class="container">
			<header class="header">
				<h1>My Projects</h1>
			</header>
			
			<div class="content">
				<aside class="col-sm-4 col-md-3 sidebar">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="{{env('APP_URL')}}">Home</a> </li>
					</ul>
				</aside>
				
				<article class="col-sm-8 col-md-9">
					<div class="col-md-8 col-md-offset-2">
					
						<!-- START Registration form -->
						
						<div class="panel panel-form">
							<!-- Form header -->
							<div class="panel-heading">
								<h2 class="title">Registration</h2>
								<p>Already have an account? <a href="{{env('APP_URL')}}">Sign in</a>.</p>
							</div>
                            
                            
                            
							
							<div class="panel-body">
								  @if ($errors->any())
       
       <div class="row" style="color:#F00">
            <div class="m12 s12 col">
                <div class="card-panel red darken-1">
                    <div class="row">
                        <div class="col l8 white-text">
                            <h5>Error!</h5>
                           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   
@endif
                                 <form role="form" name="form1" id="form1" method="post" action="{{ route('start.store') }}"   autocomplete="off"> @csrf
									<!-- Name and gender -->
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Name</label>
                                            <div class="has-feedback">
											<input type="text" class="form-control" id="name"  name="fname" required/>
                                            <span class="fa fa-user form-control-feedback" aria-hidden="true"></span>
                                            <p class="help-block">At least 8 characters long.</p>
											</div>
										</div>
										
										 
									</div>
									
									
									
									<!-- Passwords -->
									<div class="row">
										<div class="col-sm-6 form-group">
											<label for="password1" class="control-label">Password <span class="required-field">*</span></label>
											<div class="has-feedback">
												<input type="password" class="form-control" id="password1"  name="passkey"required />
												<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
											</div>
											<p class="help-block">At least 6 characters long.</p>
										</div>
										
										<div class="col-sm-6 form-group">
											<label for="password2" class="control-label">Confirm password <span class="required-field">*</span></label>
											<div class="has-feedback">
												<input type="password" class="form-control" id="password2" name="password2" required/>
												<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
											</div>
										</div>
									</div>
									
									
									<!-- Email -->
									<div class="form-group">
										<label for="email" class="control-label">Email <span class="required-field">*</span></label>
										<div class="has-feedback">
											<input type="email" class="form-control" id="email" name="email"/>
											<span class="fa fa-envelope form-control-feedback" aria-hidden="true"></span>
										</div>
									</div>
									 
									 <!-- Phone Number -->
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Phone Number <span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<input type="text" class="form-control" id="phoneno" name="phoneno" required/>
                                            <span class="fa fa-user form-control-feedback" aria-hidden="true"></span>
											</div>
										</div>
										
										 
									</div>
									 
									
									<!-- Agree static text -->
									<div class="form-group">
										<p class="form-control-static">
											Do you agree to the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>,
											and terms incorporated therein?
										</p>
									</div>
									
									<!-- Create button -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Agree and Create Account</button>
									</div>
								</form>
							</div>
							
							<!-- Form footer -->
							<div class="panel-footer">
								<span class="required-field">*</span> - required field
							</div>
						</div>
						
						<!-- END Registration form -->
					
					</div>
				</article>
				
				<div class="clearfix"></div>
			</div>
		</div>
		
		<!-- Required - jQuery -->
		<script src="{{asset('js/bootstrap.min.js') }}"></script>
		
		<!-- Required - Custom select -->
		<script src="{{asset('js/fancySelect.js') }}"></script>
		
		<script>
			$(document).ready(function() {
				$('.custom-select').fancySelect(); // Custom select
				$('[data-toggle="tooltip"]').tooltip() // Tooltip
			});
		</script> 
	</body>

 
</html>