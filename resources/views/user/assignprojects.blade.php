@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong> Assigned Projects</strong>  
                             
							</div>
                            
                            
							<div class="row">
                           
								
                             
						 
										<div class="table-responsive">
                                        
						    <table class="table" border="1" width="100%">
    <thead>
      <tr>
        <th>S/N</th>
        <th>Name</th>
         <th>Description</th>
           <th>Status</th>
            <th>User</th>
           <th>Category</th>
        <th>Action</th>
          
        </tr>
    </thead>
    <tbody style="font-size:13px">
	@foreach ($projects  as $project )					  
@php
            $projcat = $project->category;
            
   @endphp         
<tr>
        <td>{{$loop->iteration}}</td>
        <td>{{ $project->pname }}   </td>
         <td>{{ $project->pdesc }}   </td>
        <td>{{ $project->pstatus }}</td>
         <td>{{ $project->fname }}</td>
          <td>{{ $project->category }}</td>
         <td>
         
         @php
         if ($projcat <> 'Viewer') { 
         
         @endphp
         <a href="viewproj/{{ $project->pid }}"><i class="fa fa-eye"></i>View</a>  | <a href="editproj/{{ $project->pid }}"><i class="fa fa-edit"></i>Edit</a> 
          @php }else{   
          echo 'NA';
          }
          
          @endphp
         </td>
      </tr>
       @endforeach  
	
    </tbody>
  </table>

</div>
									</div>
							 
                          
                            
                          
							
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				