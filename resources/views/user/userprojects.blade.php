@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>Projects Assigned to Users</strong>  
                             
							</div>
                            
                            
							<div class="row">
                           
								
                             
						 
										<div class="table-responsive">
                                        
						    <table class="table" border="1" width="100%">
    <thead>
      <tr>
        <th>S/N</th>
        <th>Name</th>
         <th>Description</th>
           <th>Status</th>
            <th>User</th>
           <th>Category</th>
        <th>Action</th>
          
        </tr>
    </thead>
    <tbody style="font-size:13px">
	@foreach ($projects  as $project )					  

<tr>
        <td>{{$loop->iteration}}</td>
        <td>{{ $project->pname }}   </td>
         <td>{{ $project->pdesc }}   </td>
        <td>{{ $project->pstatus }}</td>
         <td>{{ $project->fname }}</td>
          <td>{{ $project->category }}</td>
        <td><a href="removeproj/{{ $project->puid }}"> <i class="fa fa-trash"></i> Remove</a>  </td>
      </tr>
       @endforeach  
	
    </tbody>
  </table>

</div>
									</div>
							 
                             <hr />
                             <h4>Add User to Project</h4>
                          
						<form role="form" name="form1" id="form1" method="post" action="{{ route('newprojuser') }}"  autocomplete="off"> @csrf
						@if ($errors->any())
       
       <div class="row" style="color:#F00">
            <div class="m12 s12 col">
                <div class="card-panel red darken-1">
                    <div class="row">
                        <div class="col l8 white-text">
                            <h5>Error!</h5>
                           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   
@endif		
									
									 
							<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Name<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="projects" id="projects" >
<option value="">Select Project </option>  
@foreach ($myprojects  as $myproject )
<option value="{{ $myproject->pid }}">{{ $myproject->pname }}</option> 
    @endforeach  
	 
                                                     
  </select>
											</div>
										</div>
										
						<div class="col-sm-12 form-group">
											<label for="name" class="control-label">User<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="users" id="users" >
<option value="">Select Users </option> 
 @foreach ($pusers  as $puser )
<option value="{{ $puser->id }}">{{ $puser->fname }}</option> 
 
	     @endforeach  
                                                     
  </select>
											</div>
										</div>				 
									</div>		
									 
									
			<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">User Category<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="ucat" id="ucat" >
<option value="">User Category  </option>  
<option value="Viewer">Viewer</option> 
<option value="Author">Author</option> 
 
	 
                                                     
  </select>
											</div>
										</div>
										
										 
									</div>						<!-- Create button -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Add User to Project</button>
									</div>
								</form>	
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				