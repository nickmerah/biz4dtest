@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>Projects Assigned to Users</strong>  
                             
							</div>
                            
                            
							<div class="row">
                           
								
                             
						 
										<div class="table-responsive">
                                        
						    <table class="table" border="1" width="100%">
    <thead>
      <tr>
        <th>S/N</th>
        <th>Project Name</th>
         <th>Question</th>
           <th>Answer</th>
            <th>Date Taken</th>
            
          
        </tr>
    </thead>
    <tbody style="font-size:13px">
	 				  
	@foreach ($surveydatas  as $surveydata )		
<tr>
        <td>{{$loop->iteration}}</td>
        <td>{{ $surveydata->pname }}   </td>
         <td>{{ $surveydata->question }}   </td>
        <td>{{ $surveydata->answer }}</td>
           <td>{{ $surveydata->date_created }}</td> 
      </tr>
        @endforeach  
	
    </tbody>
  </table>

</div>
									</div>
							 
                             <hr />
                             <h4>Take Survey</h4>
                          
						<form role="form" name="form1" id="form1" method="post" action="{{ route('surveytest') }}"  autocomplete="off"> @csrf
						@if ($errors->any())
       
       <div class="row" style="color:#F00">
            <div class="m12 s12 col">
                <div class="card-panel red darken-1">
                    <div class="row">
                        <div class="col l8 white-text">
                            <h5>Error!</h5>
                           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   
@endif		
<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Name<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="projects" id="projects" >
<option value="">Select Project </option>  
@foreach ($myprojects  as $myproject )
<option value="{{ $myproject->pid }}">{{ $myproject->pname }}</option> 
    @endforeach  
	 
                                                     
  </select>
											</div>
										</div>
                                        
                                        
								@foreach ($questions  as $question )			
									 
							<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label"><strong>Question {{$loop->iteration}}</strong> </label><input name="ques[{{$loop->iteration}}]" type="hidden" value="{{ $question->qid }}" />
                                            <div class="has-feedback">
											 {{ $question->question }}
											</div>
										</div>
										
						<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Option<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="options[{{$loop->iteration}}]" id="options" >
<option value="">Select Option </option> 
<option value="Strongly Agree">Strongly Agree </option> 
<option value="Agree">Agree </option> 
<option value="Neither agree or disagree">Neither agree or disagree </option> 
<option value="Disagree">Disagree </option> 
<option value="Strongly disagree">Strongly disagree </option> 
 
                                                     
  </select>
											</div>
										</div>				 
									</div>		
									 
								 @endforeach  	
			 					<!-- Create button -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Submit</button>
									</div>
								</form>	
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				