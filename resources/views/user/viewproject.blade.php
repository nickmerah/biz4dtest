@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>View Projects</strong>   <br />
                                 
                             
							</div>
                            
                            
							<div class="row">
                            
                           
							 
@foreach ($projects  as $project )
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Name</label>
                                            <div class="has-feedback">
										 {{ $project->pname }} 
                                    
                                           
										  </div>
										</div>
										
										 
									</div>
									
									
									
									
									
								
					    <div class="form-group">
										<label for="desc" class="control-label">Description </label>
										<div class="has-feedback">
                                           {{ $project->pdesc }}
											
	    </div>
									</div>
                                    
                                    
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Upload Image</label>
										<div class="has-feedback">
                                                {{ $project->pimage }}
	    </div>
									</div>
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Upload Video </label>
										<div class="has-feedback">
                                                {{ $project->pvideo }}
									</div>
                                    </div>
                                    
                                    <div class="form-group">
										<label for="desc" class="control-label">Project Document (Optional)</label>
										<div class="has-feedback">
                                            {{ $project->pdoc }}
	    </div>
									</div>
									 
									 <!-- Phone Number -->
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Github link </label>
                                            <div class="has-feedback">
											{{ $project->gitlink }}
											</div>
										</div>
										
										 
									</div>
									 
							<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Status</label>
                                            <div class="has-feedback">
											{{ $project->pstatus }}
											</div>
										</div>
										
										 
									</div>		
									 
									
									  @endforeach 
						 
                             
						 <div class="form-group text-center">
                         <a href="../projects" class="btn btn-primary">Back to  Projects</a>
										 
									</div>
										
									</div>
							 
							
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				