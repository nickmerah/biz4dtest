@extends('user/layout')


@section('contents')	

  
						<form role="form" name="form1" id="form1" method="post" action="{{ route('changepass') }}"   autocomplete="off"> @csrf	
							<!-- Default, grey -->
							<div class="alert alert-default alert-dismissible" role="alert">
								 
								<strong>Change Password</strong>  <br />
                                You can update your Password below
							</div>
                            
                            @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                {{ $error }} 
            @endforeach
        </ul>
    </div>
@endif
							<div class="row">
										<div class="col-sm-6 form-group">
											<label for="password1" class="control-label">Password <span class="required-field">*</span></label>
											<div class="has-feedback">
												<input type="password" class="form-control" id="password1"  name="passkey"required />
												<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
											</div>
											<p class="help-block">At least 6 characters long.</p>
										</div>
										
										<div class="col-sm-6 form-group">
											<label for="password2" class="control-label">Confirm password <span class="required-field">*</span></label>
											<div class="has-feedback">
												<input type="password" class="form-control" id="password2" name="password2" required/>
												<span class="fa fa-lock form-control-feedback" aria-hidden="true"></span>
											</div>
										</div>
									</div>
							 
							
							 <div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Change Password</button>
									</div>
						</fieldset>
                          </form>
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				