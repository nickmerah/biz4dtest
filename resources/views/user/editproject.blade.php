@extends('user/layout')


@section('contents')	

  
						
					
							<div class="alert alert-success alert-dismissible" role="alert">
								 
								<strong>Edit Project</strong>   <br />
                               You can update your Project details 
                             
							</div>
                            
                            
							<div class="row">
                            
                           
							 <form role="form" name="form1" id="form1" method="post" action="{{ route('upproj') }}"  autocomplete="off"> @csrf
						@if ($errors->any())
       
       <div class="row" style="color:#F00">
            <div class="m12 s12 col">
                <div class="card-panel red darken-1">
                    <div class="row">
                        <div class="col l8 white-text">
                            <h5>Error!</h5>
                           <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
   
@endif			@foreach ($projects  as $project )
									<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Name</label>
										  <input type="hidden" name="pid" id="pid" value="{{ $project->pid }}" />
                                            <div class="has-feedback">
											<input  name="pname" type="text" class="form-control" id="pname" value="{{ $project->pname }}" required/>
                                    
                                           
										  </div>
										</div>
										
										 
									</div>
									
									
									
									
									
								
					    <div class="form-group">
										<label for="desc" class="control-label">Description <span class="required-field">*</span></label>
										<div class="has-feedback">
                                            <textarea name="pdesc" class="form-control" id="pdesc" required> {{ $project->pdesc }} </textarea>
											
	    </div>
									</div>
                                    
                                    
                                    
                                    <div class="form-group">
                                      <div class="has-feedback"></div>
									</div>

                                    <!-- Phone Number -->
							  <div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Github link<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<input type="url" class="form-control" id="gitlink" name="gitlink"  value="{{ $project->gitlink }}" required />
                                            
											</div>
										</div>
										
										 
									</div>
									 
							<div class="row">
										<div class="col-sm-12 form-group">
											<label for="name" class="control-label">Project Status<span class="required-field">*</span></label>
                                            <div class="has-feedback">
											<select class="form-control" required name="pstatus" id="pstatus" >
<option value="{{ $project->pstatus }}">{{ $project->pstatus }} </option>  
<option value="Active">Active</option> 
<option value="Incomplete">Incomplete</option> 
<option value="Ongoing">Ongoing</option> 
<option value="Stuck">Stuck</option> 
	 
                                                     
  </select>
											</div>
										</div>
										
										 
									</div>		
									 
									  @endforeach 
									<!-- Create button -->
									<div class="form-group text-center">
										<button type="submit" class="btn btn-primary">Update Project</button>
									</div>
								</form>
                             
						 
                         
                        <hr />
                         UPLOAD NEW IMAGE<br />
                         <form role="form" name="form1" id="form1" method="post" action="{{ route('upproj') }}"  autocomplete="off"> @csrf
						 
       	@foreach ($projects  as $project )
<div class="row">
										<div class="col-sm-12 form-group"></div>
									</div>
									<div class="form-group">
									  <div class="has-feedback">
									    <input name="pimage" type="file" id="pimage"   class="form-control" accept="image/jpeg">
	                                    <span class="col-sm-12 form-group">
	                                    <input type="hidden" name="pid" id="pid" value="{{ $project->pid }}" />
                                      </span><button type="submit" class="btn btn-primary">Upload Image </button></div>
						   </div>
                                    
                                    <div class="form-group"></div>
                            <div class="row">
                                      <div class="col-sm-12 form-group"> </div>
										
										 
						   </div>		
									 
									  @endforeach 
									<!-- Create button -->
									 
							  </form>
										
					  <hr />
                         UPLOAD NEW VIDEO<br />
                         <form role="form" name="form1" id="form1" method="post" action="{{ route('upproj') }}"  autocomplete="off"> @csrf
						 
       	@foreach ($projects  as $project )
<div class="row">
										<div class="col-sm-12 form-group"></div>
									</div>
									<div class="form-group">
									  <div class="has-feedback">
									    <input name="pvideo2" type="file" id="pvideo2"   class="form-control"  accept="video/x-mpeg2" />
									    <span class="col-sm-12 form-group">
	                                    <input type="hidden" name="pid" id="pid" value="{{ $project->pid }}" />
                                      </span>
									    <button type="submit" class="btn btn-primary">Upload Video </button></div>
						   </div>
                                    
                                    <div class="form-group"></div>
                            <div class="row">
                                      <div class="col-sm-12 form-group"> </div>
										
										 
						   </div>		
									 
									  @endforeach 
									<!-- Create button -->
									 
							  </form>				</div>
							 
							     	
							
						</fieldset>
                     
								</div>
							</div>
						</div>
						
					 
						 
						
					</div>
					
					<!-- END Login form -->
					
				</article>
                @endsection		
				
				