<!doctype html>
<html lang="en-US">
	
 
<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
		<title>.:: My Projects</title>
		
		<!-- Optional - Google font -->
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Gafata" />
		
		<!-- Required - Icon font -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css') }} " />
		
		<!-- Required - Bootstrap CSS -->
		<link rel="stylesheet" href="{{asset('css/bootstrap.min.css') }}" /> 
		
		<!-- Required - Form style -->
		<link rel="stylesheet" href="{{asset('css/flat-form.css') }}" /> 
		
		<!-- NOT required - Page style -->
<link rel="stylesheet" href="{{asset('css/page-style.css') }}" /> 
<script type="text/javascript" src="{{asset('js/jquery-1.11.3-jquery.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/validation.min.js') }}"></script>
<script type="text/javascript" src="{{asset('js/script.js') }}"></script>
	</head>
	
	<body>
     @empty(session('status'))
           {!  <script type="text/javascript">
			alert("Session expired!\nYou will be redirected to the home page shortly...");
			window.location = "{{env('APP_URL')}}";
		</script>  !}
    
@endempty   
		<div class="container">
			<header class="header">
				<h1>My Projects</h1>
			</header>
			
			<div class="content">
				<aside class="col-sm-4 col-md-3 sidebar">
					<ul class="nav nav-pills nav-stacked">
						<li class="active"><a href="{{env('APP_URL')}}">Home</a> </li>
					</ul>
				</aside>
				
				<article class="col-sm-8 col-md-9">
					<div class="col-md-8 col-md-offset-2">
					
						<!-- START Registration form -->
						
						<div class="panel panel-form">
							<!-- Form header -->
							 
                            
                            
                            
							
							<div class="panel-body">
								   
                                  <form name="form1" id="form1"  enctype="multipart/form-data" autocomplete="off">
        <div class="row">
          <div class="input-field col s12 center">
             
          <p class="center login-form-text" style = "font-weight: bold; color: #104E8B; font-size: 25px;">  REGISTRATION</p>
                <p class="center login-form-text" style = "color:#090; font-size: 25px; font-weight:bold">
                
           
                
                @if (session('status'))
    
        {{ session('status') }}
  
@endif </p>
                 <p style="font-size:14px">Your Account has Been Successfully Created.</p>
    <p style="font-size:14px">Your Account Info  is as follows:<br><br>
      <span id="ss"><b>Email Address</b>: @if (session('email'))
    
        {{ session('email') }}
  
@endif <br>
      <b>Password</b>: @if (session('passkey'))
    
        {{ session('passkey') }}
  
@endif </span><br><br>
    Please note these down and do not share your password with anyone.<br><br>A copy of this info will be sent to your email address at  {{ session('email') }} <br><br>If you cannot find the email in your Inbox, do check your Spam</p>
	<div> 
	  
	</div>
          </div>
        </div>
       
       
       
       
       
     
        
          <div class="row margin">
          <div class="input-field col s12">
           
          <a href="{{env('APP_URL')}}">  <input type= "button" name = "submit" value = "CLICK TO CONTINUE"  style = "width: 100%; padding: 9px; background: #1C86EE; color: white; border: 0px; " > </a>
          <br><br>
          </div>
        </div>
        
        </form>
							</div>
							
						 
					
					</div>
				</article>
				
				<div class="clearfix"></div>
			</div>
		</div>
		
		<!-- Required - jQuery -->
		<script src="{{asset('js/bootstrap.min.js') }}"></script>
		
		<!-- Required - Custom select -->
		<script src="{{asset('js/fancySelect.js') }}"></script>
		
		<script>
			$(document).ready(function() {
				$('.custom-select').fancySelect(); // Custom select
				$('[data-toggle="tooltip"]').tooltip() // Tooltip
			});
		</script> 
	</body>

 
</html>