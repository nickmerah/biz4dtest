<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic as Image;
use Session;

class UserController extends Controller
{
	
	public function __construct(Request $request)
    {
		if(!$request->session()->exists('id'))
        {
             echo '<script type="text/javascript">
			alert("What Exactly is your Wish!");
			window.location = "../public";
		</script>';
            exit(); 
        }
 
         
    }
    
    public function index(Request $request)
    {
 			 
 	 $id =   $request->session()->get('id');  
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
					
		$projdata = DB::table('projectuser')
		 			->join('projects', 'projects.pid', '=', 'projectuser.pid')
					->join('login', 'login.id', '=', 'projectuser.userid')
					 ->where('userid', $id)
					->count(); 
					
		$users = DB::table('login')
					->where('status', 1)
					->where('id', '!=', $id)
					->count();
					
 					
		 $projdatas = DB::select(DB::raw('select b.pname,count(a.pid) AS count  from projectuser a,
projects b where a.pid=b.pid group by a.pid'));
			 
$surveydatas = DB::select(DB::raw('select q.qid,a.answer, count(a.answer) AS count  from answers a,
questions q where a.question_id=q.qid group by a.answer'));
 
			 
			 	
       return view('user/home', ['users' => $userdata, 'numproj' => $projdata, 'numusers' => $users, 'projs' => $projdatas, 'surveys' => $surveydatas]); 
        
		//
    }
	
	
	public function changepassword(Request $request)
    {
		  $id =   $request->session()->get('id');  
		 
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		  
       return view('user/changepass', ['users' => $userdata]); 
    }
	
	
	
	  public function changepass(Request $request)
    {
        //
		$rules =  [
             
			 'passkey' => 'required|min:6',
        ] ;
    
 
	$validator = Validator::make($request->all(),$rules,$messages = [
    
	 
	 'passkey.required' => 'Password is required !',
	'passkey.min' => 'Password Should not be less than 6 characters!',
 
	 
] );

		if ($validator->fails()) {
			return redirect('changepassword')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
			
			$passkey   = $data['passkey'];
			 $passkey2  = $data['password2'];
			 
			 	if ($passkey <> $passkey2) {
				 return redirect('changepassword')
			     ->withInput()
			     ->withErrors(['Password and confirm Password does not Match']);
}else{
	$id =   $request->session()->get('id');
}
			 
			 
			try{  
				 

					$affected = DB::table('login')
             				 ->where('id', $id)
             				 ->update([
							'passkey' =>  rtrim(ltrim(password_hash($passkey, PASSWORD_DEFAULT))),
						 
							  ]);
	
					return redirect('changepassword')->with('status',"SUCCESS")->with('message',"Password Successfully Updated");
 
				
			}
			catch(Exception $e){
				return redirect('changepassword')->withInput()
			     ->withErrors(['Error Changing Password']);
			}
		}


		
	 
    }
	
	public function projects(Request $request)
    {
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					->where('uid', $id)
					->select('*' )
					->get(); 
		  
       return view('user/projects', ['users' => $userdata, 'projects' => $projdata]); 
    }
	
	
	public function newproject(Request $request)
    {
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		  
       return view('user/newproj', ['users' => $userdata]); 
    }
	
	
	
	public function newproj(Request $request)
    {
         $rules =  [
            'pname' => 'required',
            'pdesc' => 'required',
			'pimage' => 'required',
            'pvideo' => 'required',
			'gitlink' => 'required',
			  
        ] ;
    
        
	
	
	$validator = Validator::make($request->all(),$rules,$messages = [
    'pname.required' => 'Project Name is required!',
	'pdesc.required' => 'Project Description is required !',
	'pimage.required' => 'Project Image is required!',
	'pvideo.required' => 'Project Video is required!',
	'gitlink.required' => 'Github Link is required!',
 
] );
		if ($validator->fails()) {
			return redirect('newproject')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
  try{  
				  $pimagepath = $request->session()->get('id').time().getClientOriginalExtension();
				  $pvideopath = $request->session()->get('id').time().getClientOriginalExtension();
				   $pdocpath = $request->session()->get('id').time().getClientOriginalExtension();
				  $appid = DB::table('projects')->insertGetId(
    				[
   				 'pname' => addslashes(ucwords(($data['pname']))), 
				 'pdesc' => addslashes(ucwords(($data['pdesc']))),
				 'pimage' => $pimagepath,
				 'pvideo' => $pvideopath,
				 'pdoc' => $pdocpath,
				 'gitlink' => strtolower($data['gitlink']),
				 'pstatus' => $data['pstatus'],
				  'uid' => $request->session()->get('id'),
				 
			 
					]
						);
						
						 
						//image
						     $file = $request->file('pimage');
							 $id =   $request->session()->get('id');
 							 $destinationPath = public_path('/images');
     						   $request->pimage->move($destinationPath, $pimagepath);
							   
							   //video
							    $path = $request->file('pvideo')->store('videos', public_path('/images'));
							  
							   //doc
						     $doc = $request->file('pdoc');
 							 $destinationPath = public_path('/images');
     						   $request->pdoc->move($destinationPath, $pdocpath);

					return redirect('projects')->with('status',"SUCCESS")->with('message',"Project Successfully Created");
 
				
			}
			catch(Exception $e){
				return redirect('newproject')->withInput()
			     ->withErrors(['Error Creating Project']);
			}
		}
    }
	
	
	 public function viewproject(Request $request,$pid)
    {  
		 
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					->where('pid', $pid)
					->select('*' )
					->get(); 
		  
       return view('user/viewproject', ['users' => $userdata, 'projects' => $projdata]); 
         
		   
			
       
    }
	
	public function editproject(Request $request,$pid)
    {  
		 
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					->where('pid', $pid)
					->select('*' )
					->get(); 
		  
       return view('user/editproject', ['users' => $userdata, 'projects' => $projdata]); 
         
		   
			
       
    }
	
	public function upproj(Request $request)
    {
         $rules =  [
            'pname' => 'required',
            'pdesc' => 'required',
			'gitlink' => 'required',
			  
        ] ;
    
        
	
	
	$validator = Validator::make($request->all(),$rules,$messages = [
    'pname.required' => 'Project Name is required!',
	'pdesc.required' => 'Project Description is required !',
	'gitlink.required' => 'Github Link is required!',
 
] );
		if ($validator->fails()) {
			return redirect('projects')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
  try{  
				   
				   $affected = DB::table('projects')
             				 ->where('pid', $data['pid'])
             				 ->update([
    			'pname' => addslashes(ucwords(($data['pname']))), 
				 'pdesc' => addslashes(ucwords(($data['pdesc']))),
				 'gitlink' => strtolower($data['gitlink']),
				 'pstatus' => $data['pstatus'], 
					]);
					
		 
					return redirect('projects')->with('status',"SUCCESS")->with('message',"Project Successfully Updated");
 
				
			}
			catch(Exception $e){
				return redirect('projects')->withInput()
			     ->withErrors(['Error Updating Project']);
			}
		}
    }
	
	
	public function projectusers(Request $request)
    {
		   $id =   $request->session()->get('id');   
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 	 
		 $projdata = DB::table('projectuser')
		 			->join('projects', 'projects.pid', '=', 'projectuser.pid')
					->join('login', 'login.id', '=', 'projectuser.userid')
					 ->where('addedby', $id)
					->select('*' )
					->orderBy('projectuser.puid')
					->get(); 
					
					 		
					
		$projects = DB::table('projects')
					->where('uid', $id)
					->select('*' )
					->get(); 
					
		$users = DB::table('login')
					->where('status', 1)
					->where('id', '!=', $id)
					->select('*' )
					->get();
		  
       return view('user/userprojects', ['users' => $userdata, 'projects' => $projdata, 'myprojects' => $projects, 'pusers' => $users]); 
    }
	

public function newprojuser(Request $request)
    {
         $rules =  [
            'projects' => 'required',
            'users' => 'required',
			'ucat' => 'required',
             
			  
        ] ;
    
        
	
	
	$validator = Validator::make($request->all(),$rules,$messages = [
    'projects.required' => 'Project  is required!',
	'users.required' => 'Users is required !',
	'ucat.required' => 'User Category is required!',
	
 
] );
		if ($validator->fails()) {
			return redirect('projectusers')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
			
			//check no of project users category

     $numviewer = DB::table('projectuser')->where('pid', $data['projects'])->where('category', 'Viewer')->count();
	 $numauthor = DB::table('projectuser')->where('pid', $data['projects'])->where('category', 'Author')->count();
	 
	 if ($numviewer > 5) {
		 return redirect('projectusers')
			     ->withInput()
			     ->withErrors(['You cannot add more than 5 Viewer for a Project']); exit;
	 }
	 
	 if ($numauthor > 2) {
		 return redirect('reg')
			     ->withInput()
			     ->withErrors(['Email Has Already Been Registered']); exit;
	 }
			
  try{  
				 
				  
						
						//assign to userproject table
						$std = DB::table('projectuser')->insert(
    				[
   				  'pid' => $data['projects'],
    			  'userid' => $data['users'],
				  'addedby' => $request->session()->get('id'),
    			  'category' => $data['ucat'],  
					]
						);	 

					return redirect('projectusers')->with('status',"SUCCESS")->with('message',"Project User Successfully Added");
 
				
			}
			catch(Exception $e){
				return redirect('projectusers')->withInput()
			     ->withErrors(['Error Adding Project Users']);
			}
		}
    }
	
	public function removeproj($id)
    {
          
			try{  
			  
             DB::table('projectuser')->where('puid', $id)->delete();					
			 return redirect('projectusers')->with('status',"Project User Removed Successfully");

				
			}
			catch(Exception $e){
				return redirect('projectusers')->withInput()
			     ->withErrors(['Error Removing Project User']);
			 
		}
    }
	
	public function assignprojects(Request $request)
    {
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projectuser')
		 			->join('projects', 'projects.pid', '=', 'projectuser.pid')
					->join('login', 'login.id', '=', 'projectuser.userid')
					 ->where('userid', $id)
					->select('*' )
					->orderBy('projectuser.puid')
					->get();  
					
					
		  
       return view('user/assignprojects', ['users' => $userdata, 'projects' => $projdata]); 
    }
	
	
	public function survey(Request $request)
    {
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('login')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		  $surveydata = DB::table('questions')
					->select('*' )
					->get();  
					
			 $projdata = DB::table('projectuser')
		 			->join('projects', 'projects.pid', '=', 'projectuser.pid')
					->join('login', 'login.id', '=', 'projectuser.userid')
					 ->where('userid', $id)
					->select('*' )
					->orderBy('projectuser.puid')
					->get();  
			
			 $surveydata = DB::table('answers')
		 			->join('questions', 'questions.qid', '=', 'answers.question_id')
					->join('projects', 'projects.pid', '=', 'answers.pid')
					 ->where('user_id', $id)
					->select('*' )
					->orderBy('questions.qid')
					->get();  
		  
       return view('user/survey', ['users' => $userdata,'questions' => $surveydata,'myprojects' => $projdata, 'surveydatas' => $surveydata ]); 
    }
	
	public function logout(Request $request)
    {
        //
		$request->session()->forget('id');
		 Session::flush();
		 return redirect('/')->withErrors(['Your logout was successfully']);;
    }
	
	
  
   public function surveytest(Request $request)
    {
          
            $data = $request->input();
 
			
  try{  
				 
				  
						
						//assign to userproject table
						 for ($i = 1; $i <= 5 ; $i++) {
				 
				  $sid = DB::table('answers')->insert(
    				[
   				 'user_id' => $request->session()->get('id'),
				  'pid' => $data['projects'],
    			 'question_id' => $data['ques'][$i],
				'answer' => $data['options'][$i] 
					]
						);
				  }

					return redirect('survey')->with('status',"SUCCESS")->with('message',"Survey Taken Successfully ");
 
				
			}
			catch(Exception $e){
				return redirect('projectusers')->withInput()
			     ->withErrors(['Error Adding Project Users']);
			}
	}
   public function admindashboard(Request $request)
    {
 			 
 	 $id =   $request->session()->get('id');  
		$userdata = DB::table('alogin')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
					
		$projdata = DB::table('projectuser')
		 			->join('projects', 'projects.pid', '=', 'projectuser.pid')
					->join('login', 'login.id', '=', 'projectuser.userid')
					->count(); 
					
		$users = DB::table('login')
					->where('status', 1)
					->where('id', '!=', $id)
					->count();
					
 					
		 $projdatas = DB::select(DB::raw('select b.pname,count(a.pid) AS count  from projectuser a,
projects b where a.pid=b.pid group by a.pid'));
			 
$surveydatas = DB::select(DB::raw('select q.qid,a.answer, count(a.answer) AS count  from answers a,
questions q where a.question_id=q.qid group by a.answer'));
 
			 
			 	
       return view('admin/home', ['users' => $userdata, 'numproj' => $projdata, 'numusers' => $users, 'projs' => $projdatas, 'surveys' => $surveydatas]); 
        
		//
    }
	
	public function aprojects(Request $request)
    {
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('alogin')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					 
					->select('*' )
					->get(); 
		  
       return view('admin/projects', ['users' => $userdata, 'projects' => $projdata]); 
    }
	
	 public function aviewproject(Request $request,$pid)
    {  
		 
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('alogin')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					->where('pid', $pid)
					->select('*' )
					->get(); 
		  
       return view('admin/viewproject', ['users' => $userdata, 'projects' => $projdata]); 
         
 
    }
	
	public function aeditproject(Request $request,$pid)
    {  
		 
		  $id =   $request->session()->get('id');  
		
		$userdata = DB::table('alogin')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
		 $projdata = DB::table('projects')
					->where('pid', $pid)
					->select('*' )
					->get(); 
		  
       return view('admin/editproject', ['users' => $userdata, 'projects' => $projdata]); 
         
 
    }
	
	public function aupproj(Request $request)
    {
         $rules =  [
            'pname' => 'required',
            'pdesc' => 'required',
			'gitlink' => 'required',
			  
        ] ;
    
        
	
	
	$validator = Validator::make($request->all(),$rules,$messages = [
    'pname.required' => 'Project Name is required!',
	'pdesc.required' => 'Project Description is required !',
	'gitlink.required' => 'Github Link is required!',
 
] );
		if ($validator->fails()) {
			return redirect('aprojects')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
  try{  
				   
				   $affected = DB::table('projects')
             				 ->where('pid', $data['pid'])
             				 ->update([
    			'pname' => addslashes(ucwords(($data['pname']))), 
				 'pdesc' => addslashes(ucwords(($data['pdesc']))),
				 'gitlink' => strtolower($data['gitlink']),
				 'pstatus' => $data['pstatus'], 
					]);
					
		 
					return redirect('aprojects')->with('status',"SUCCESS")->with('message',"Project Successfully Updated");
 
				
			}
			catch(Exception $e){
				return redirect('aprojects')->withInput()
			     ->withErrors(['Error Updating Project']);
			}
		}
    }
	
	public function deleteproj($id)
    {
          
			try{  
			  
             DB::table('projects')->where('pid', $id)->delete();					
			 return redirect('aprojects')->with('status',"Project Removed Successfully");

				
			}
			catch(Exception $e){
				return redirect('aprojects')->withInput()
			     ->withErrors(['Error Removing Project']);
			 
		}
    }
	
	public function aprojectusers(Request $request)
    {
		   $id =   $request->session()->get('id');   
		
		$userdata = DB::table('alogin')
					->where('status', 1)
					->where('id', $id)
					->select('*' )
					->get();
	 
					
		$users = DB::table('login')
					->where('status', 1)
					->select('*' )
					->get();
		  
       return view('admin/userprojects', ['users' => $userdata,  'pusers' => $users]); 
    }
    public function removeuser($id)
    {
          
			try{  
			  
             DB::table('login')->where('id', $id)->delete();					
			 return redirect('aprojectusers')->with('status',"User Removed Successfully");

				
			}
			catch(Exception $e){
				return redirect('aprojectusers')->withInput()
			     ->withErrors(['Error Removing User']);
			 
		}
    }
	
}
