<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Start;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotifier;

class StartController extends Controller
{
   
    public function store(Request $request)
    {
         $rules =  [
            'passkey' => 'required|min:6',
            'fname' => 'required|string|min:8|max:150',
            'phoneno' => 'required|numeric|min:11',
			  'email' => 'required|string|email|max:150',
			  
        ] ;
    
        
	
	
	$validator = Validator::make($request->all(),$rules,$messages = [
    'passkey.required' => 'Password is required !',
	'passkey.min' => 'Password Should not be less than 6 characters!',
	'fname.required' => 'Surname is required !',
	'fname.min' => 'Surname Should not be less than 8 characters!',
	'phoneno.required' => 'Phone No is required !',
	'phoneno.min' => 'Phone No Should not be less than 11 characters!',
	 
	'email.required' => 'Email is required !',
	'email.min' => 'Email Should not be more than 150 characters!',
 
] );
		if ($validator->fails()) {
			return redirect('reg')
			->withInput()
			->withErrors($validator);
		}else{
            $data = $request->input();
			 
			 
			 $passkey   = $data['passkey'];
			 $passkey2  = $data['password2'];
             
				if ($passkey <> $passkey2) {
				 return redirect('reg')
			     ->withInput()
			     ->withErrors(['Password and confirm Password does not Match']);
}elseif(strlen($data['phoneno']) > 11 ){
	 return redirect('reg')
			     ->withInput()
			     ->withErrors(['Phone Number cannot be greater than 11 characters']);
}else{
	 //check if parameters are duplicated
	 $numemail = DB::table('login')->where('email', $data['email'])->count();
}
 
	 if ($numemail == 1) {
		 return redirect('reg')
			     ->withInput()
			     ->withErrors(['Email Has Already Been Registered']); exit;
	 }
 
  try{  
				 
				  $appid = DB::table('login')->insertGetId(
    				[
   				 'fname' => addslashes(strtoupper(($data['fname']))), 
				'email' => strtolower($data['email']),
				'phoneno' => $data['phoneno'],
				'passkey' => rtrim(ltrim(password_hash($data['passkey'], PASSWORD_DEFAULT))),
			 
					]
						);
						
				 
		 //send mail
		 
		 $dmail = strtolower($data['email']);
		 $passkey = rtrim(ltrim($data['passkey']));
		 $fname = addslashes(strtoupper(($data['fname'])));
		 $today = date("j/m/Y, H:m");
				
				$details = [  
						'title'=>'Your Account Info from My Projects',
						'body'=>"Mail sent on $today",
						'info'=>"Your Account Info for My Projectsis as follows:",
						'email'=>$dmail,
						'passkey'=>$passkey,
						'fullnames'=>$fname
						];
 			 
            /***commented out cos its localhosts 
			 Mail::to($dmail)->send(new MailNotifier($details));***/

					return redirect('success')->with('status',"SUCCESS")->with('email',$data['email'])->with('passkey',$data['passkey']);
 
				
			}
			catch(Exception $e){
				return redirect('reg')->withInput()
			     ->withErrors(['Error Creating Account']);
			}
		}
    }
      
    public function login(Request $request)
    {
        $data = $request->input();
		 $email   = filter_var($data['email'], FILTER_SANITIZE_EMAIL);
		$password  = strtolower($data['password']);
	
		$rules =  [
            'password' => 'required',
			  'email' => 'required|email',
			  
        ] ;
 
	$validator = Validator::make($request->all(),$rules,$messages = [
    'password.required' => 'Password is required !',
	'email.required' => 'Email is required !', 
] ); 

if ($validator->fails()) {
			return redirect('/')
			->withInput()
			->withErrors($validator);
		}else{
			
  	 $credentials = DB::table('login')->select('id', 'passkey')->where('email', $email)->first();
	
	 
	if ( isset ($credentials) and (password_verify( $password,$credentials->passkey))) {
 
            $request->session()->regenerate(); 
		    $request->session()->put('id', $credentials->id);
		 
 			return redirect()->intended('dashboard');
        }else{
        return back()->withInput()->withErrors([
            'Error: Wrong Email/Password, Please try Again',
        ]);
		}
    } 
	}
	
	
	public function adminlogin(Request $request)
    {
        $data = $request->input();
		 $username   = filter_var($data['username'], FILTER_SANITIZE_STRING);
		$password  = strtolower($data['password']);
	
		$rules =  [
            'password' => 'required',
			  'username' => 'required',
			  
        ] ;
 
	$validator = Validator::make($request->all(),$rules,$messages = [
    'password.required' => 'Password is required !',
	'username.required' => 'Username is required !', 
] ); 

if ($validator->fails()) {
			return redirect('/alogin')
			->withInput()
			->withErrors($validator);
		}else{
			
  	 $credentials = DB::table('alogin')->select('id', 'passkey')->where('username', $username)->first();
	
	 
	if ( isset ($credentials) and (password_verify( $password,$credentials->passkey))) {
 
            $request->session()->regenerate(); 
		    $request->session()->put('id', $credentials->id);
		 
 			return redirect()->intended('admindashboard');
        }else{
        return back()->withInput()->withErrors([
            'Error: Wrong Username/Password, Please try Again',
        ]);
		}
    } 
	}
	
	

	 
}

